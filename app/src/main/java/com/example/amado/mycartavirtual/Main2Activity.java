package com.example.amado.mycartavirtual;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class Main2Activity extends Activity implements View.OnClickListener {

    private CardView mapaCard, menuCard, reservaCard, servicioCard;
    private static final String whatsApp = "com.whatsapp";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mapaCard = (CardView) findViewById(R.id.mapa_card);
        menuCard = (CardView) findViewById(R.id.menu_card);
        reservaCard = (CardView) findViewById(R.id.reserva_card);
        servicioCard = (CardView) findViewById(R.id.servicio_card);

        mapaCard.setOnClickListener(this);
        menuCard.setOnClickListener(this);
        reservaCard.setOnClickListener(this);
        servicioCard.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId())
        {
            case R.id.menu_card : intent = new Intent(this, CartaActivity.class);
                startActivity(intent); break;
            case R.id.mapa_card : intent = new Intent(this, MapsActivity.class);
                startActivity(intent); break;
            case R.id.reserva_card :
                PackageManager pm = getPackageManager();
                intent = pm.getLaunchIntentForPackage(whatsApp);
                startActivity(intent);
                break;
            case R.id.servicio_card : intent = new Intent(this, ServicioActivity.class);
                startActivity(intent);break;
            default:break;
        }
    }
}
