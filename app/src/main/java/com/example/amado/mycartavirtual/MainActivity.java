package com.example.amado.mycartavirtual;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private static final String whatsApp = "com.whatsapp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    void ConsultarUbicacion (View v)
    {
        Intent intencion = new Intent( this, MapsActivity.class);
        startActivity(intencion);
    }
    void verMenu (View v)
    {
        Intent intencion = new Intent( this, CartaActivity.class);
        startActivity(intencion);
    }
    void reservarMesa (View v)
    {
        PackageManager pm = getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(whatsApp);
        startActivity(intent);
    }
    void calificarServicio (View v)
    {
        Intent intencion = new Intent( this, ServicioActivity.class);
        startActivity(intencion);
    }
}
