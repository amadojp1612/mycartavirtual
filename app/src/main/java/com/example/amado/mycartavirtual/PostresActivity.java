package com.example.amado.mycartavirtual;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PostresActivity extends ListActivity
{
    private MyAdapter mAdapter = null;

    public class Node
    {
        public String mTitle;
        public String mPrice;
        public Integer mImageResource;
    }
    private static ArrayList<Node> mArray = new ArrayList<Node>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setData();

        mAdapter = new MyAdapter(this);
        setListAdapter(mAdapter);
    }
    protected void onListItemClick (ListView listView,View view, int position, long id)
    {
        Toast.makeText(this, mArray.get(position).mPrice, Toast.LENGTH_SHORT).show();
    }
    private void setData ()
    {
        mArray.clear();
        Node myNode = new Node();
        Node myNode2 = new Node();
        Node myNode3 = new Node();

        //Postre_1
        myNode.mTitle = this.getResources().getString(R.string.Postre1);
        myNode.mImageResource = R.drawable.postre1;
        myNode.mPrice = "9,000 COP";
        mArray.add(myNode);

        //Postre_2
        myNode2.mTitle = this.getResources().getString(R.string.Postre2);
        myNode2.mImageResource = R.drawable.postre2;
        myNode2.mPrice = "9,500 COP";
        mArray.add(myNode2);

        //Postre_3
        myNode3.mTitle = this.getResources().getString(R.string.Postre3);
        myNode3.mImageResource = R.drawable.postre3;
        myNode3.mPrice = "10,500 COP";
        mArray.add(myNode3);
    }
    public static class MyAdapter extends BaseAdapter
    {
        private Context mContext;
        public MyAdapter (Context c)
        {
            mContext = c;
        }
        @Override
        public int getCount() {
            return mArray.size();
        }

        @Override
        public Object getItem(int position) {
            return mArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            if (convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                view = inflater.inflate(R.layout.myitem2,null);
            }
            else
            {
                view = convertView;
            }
            ImageView img = (ImageView) view.findViewById(R.id.image);
            img.setImageDrawable(mContext.getResources().getDrawable(mArray.get(position).mImageResource));

            TextView tTitle = (TextView) view.findViewById(R.id.tittle);
            tTitle.setText(mArray.get(position).mTitle);

            return view;
        }
    }
}
