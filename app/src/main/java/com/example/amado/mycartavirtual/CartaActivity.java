package com.example.amado.mycartavirtual;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toolbar;

import java.util.ArrayList;

public class CartaActivity extends ListActivity
{

    String [] productos = new String[]{"Entradas", "Platos Fuertes", "Bebidas", "Postres"};
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);


        listView =  (ListView) findViewById(android.R.id.list);
        ArrayAdapter <String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,productos);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (position == 0)
                {
                    Intent intencion = new Intent( view.getContext(), EntradaActivity.class);
                    startActivityForResult(intencion,0);
                }
                if (position == 1)
                {
                    Intent intencion = new Intent( view.getContext(), PlatoActivity.class);
                    startActivityForResult(intencion,1);
                }
                if (position == 2)
                {
                    Intent intencion = new Intent( view.getContext(), BebidasActivity.class);
                    startActivityForResult(intencion,2);
                }
                if (position == 3)
                {
                    Intent intencion = new Intent( view.getContext(), PostresActivity.class);
                    startActivityForResult(intencion,3);
                }
            }

        });
    }



}
