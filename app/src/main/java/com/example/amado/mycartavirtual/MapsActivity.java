package com.example.amado.mycartavirtual;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng laureles = new LatLng(6.243834, -75.594122);
        LatLng belen = new LatLng(6.232807, -75.603614);
        LatLng poblado = new LatLng(6.202091,-75.572060);
        mMap.addMarker(new MarkerOptions().position(laureles).title("JP Laureles").snippet("Av. Nutibara"));
        mMap.addMarker(new MarkerOptions().position(belen).title("JP Belén").snippet("Cc Los Molinos"));
        mMap.addMarker(new MarkerOptions().position(poblado).title("JP Poblado").snippet("Mall La Strada"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(6.2186297,-75.584388),(float)13.75));
    }
}
